#!/usr/bin/env python3
#
# Stormshield Network Security Filter Log Enumerator
# Version 0.1 - BETA
# Created by Luke "sludge3000" Savage
#

# Import modules
import argparse
import os
import re

from pathlib import Path


# Initialize empty working lists
templist1 = []
templist2 = []


# Tempoarary list recycler
def tlc():
    global templist1
    global templist2
    templist1 = templist2
    templist2 = []


# Simple iterator
def iter(filter):
    global templist1
    global templist2
    for line in templist1:
        if filter in line:
            templist2.append(line)


# Extract all lines into a workable list
def extract(files):
    for file in files:
        iFile = open(file)
        for line in iFile:
            templist2.append(line)
        iFile.close()


# Filter out lines containing rulename into working list
def rulename_filter(rulename):
    rname = 'rulename="' + rulename + '"'
    tlc()
    iter(rname)


# Filter out entries matching specific source IP
def source_filter(sourceip):
    source = 'src=' + sourceip
    tlc()
    iter(source)


# Enumerate unique destination IP addresses
def destination_enumerator():
    tlc()
    for line in templist1:
        # Regex for destintion IP address
        regex1 = re.search(
            r'dst=(\d+\.\d+\.\d+\.\d+)', line
            )
        # Regex for destination port
        regex2 = re.search(r'dstport=(\d+)', line)
        regex1list = []
        regex2list = []
        regex1list.append(regex1.group(1))
        regex2list.append(regex2.group(1))
        # Ensure the data from same log line is kept in a tuple
        for x in regex1list:
            for y in regex2list:
                templist2.append((x, y))


# Format and print the final results
def format_n_print(oFile):
    uniq = set(templist2)
    ordered = sorted(uniq)
    oFile = open(oFile, 'w+')
    for tup in ordered:
        detup = ','.join(tup)
        oFile.write(detup + '\n')
    oFile.close()


# Filter log enumerator function
def snsfle():
    # Set local variables
    p = Path(os.getcwd())
    files = list(p.glob('l_filter*'))

    # Local argparse Variables
    args = parser.parse_args()
    rulename = args.rulename
    sourceip = args.srcip

    # Static output filename
    oFile = rulename + '.txt'

    extract(files)

    if rulename is not None:
        rulename_filter(rulename)

    if sourceip is not None:
        source_filter()

    destination_enumerator()

    format_n_print(oFile)


# Define main function
def main():
    snsfle()


# Argparse configuration
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
parser.add_argument("-R", "--rulename", default=None,
                    help="Specify the rulename for the matching rule.")
parser.add_argument("-s", "--srcip", default=None,
                    help="Specify source IP to filter.")


# Call main funciton
main()
